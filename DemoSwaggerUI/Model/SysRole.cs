﻿
using System;
using System.ComponentModel.DataAnnotations;

namespace DemoSwaggerUI
{
    public partial class SysRole
    {
        /// <summary>
        /// 表的主键
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 角色名称
        /// </summary>
        public string F_RoleName { get; set; }
        /// <summary>
        /// 角色编码
        /// </summary>
        public string F_RoleCode { get; set; }
        /// <summary>
        /// 有效标志
        /// </summary>
        public int F_EnabledMark { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int F_Sort { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string F_Remarks { get; set; }
        /// <summary>
        /// 添加用户
        /// </summary>
        public string F_CreateName { get; set; }
        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime F_CreateTime { get; set; }
        /// <summary>
        /// 更新用户
        /// </summary>
        public string F_UpdateName { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime F_UpdateTime { get; set; }
        
    }
}
